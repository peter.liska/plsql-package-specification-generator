For /F "Tokens=1" %%I in ('git rev-parse --show-toplevel') Do Set git_path=%%I
SET git_path=%git_path%/
git diff --name-only --line-prefix=%git_path% > git_changes.txt
java -jar PackageSpecificationGenerator.jar git_changes.txt log