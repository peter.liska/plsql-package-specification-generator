package generate;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import basic.Logger;

public class Generator {

	private String actualTime = "";

	private String readPath = null;
	private String writePath = null;
	private BufferedReader reader = null;
	private BufferedWriter writer = null;

	/**
	 * Creates new Generator.
	 *
	 * @param readPath  path to read
	 * @param writePath Path to write
	 */
	public Generator(String readPath, String writePath) {
		// Set paths for reading and writing files.
		this.readPath = readPath;
		this.writePath = writePath;

		// Set DateTime
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime localDateTimeNow = LocalDateTime.now();
		this.actualTime = dateTimeFormatter.format(localDateTimeNow);
	}

	/**
	 * Beautify all lines in file.
	 *
	 * @throws IOException
	 */
	public void doWork() throws IOException {
		Logger.printInfoStart("Processing.");
		this.startFiles();

		// basic variables
		boolean createDone = false;
		boolean isPrivateSector = false;
		String functionName = "";
		boolean isFunctionHeader = false;
		List<String> listHeader = new ArrayList<>();
		boolean isFunctionBody = false;
		String packageName = "";

		// regEx
		String reStart = ".*\\b" + "(?i)";
		String reEnd = "\\b.*";

		// All lines - loop whole file.
		String line = this.reader.readLine();
		Logger.printInfoDetail("line> " + line);
		while (line != null) {

			// create
			if (!createDone) {
				if (line.matches(reStart + "CREATE" + reEnd)) {
					line = line.replaceAll("\\b(?i)" + " BODY " + "\\b", " ");
					this.writer.write(line);
					this.writer.newLine();
					this.writer.newLine();
					createDone = true;

					// Get package name
					int position = line.toLowerCase().indexOf("package");
					packageName = line.substring(position + "package".length()).trim();
					position = packageName.toLowerCase().indexOf(" ");
					if (position > 0) {
						packageName = packageName.substring(0, position).trim();
					}
				}
			}

			// dbDoc
			if (line.contains("/** @private")) {
				isPrivateSector = true;
				Logger.printInfo("Next will be PRIVATE!");
			}

			if (isFunctionHeader) {
				if (line.matches(reStart + "RETURN" + reEnd)) {
					listHeader.add(line);
					isFunctionBody = true;
				} else if (line.matches(reStart + "IS" + reEnd)) {
					isFunctionBody = true;
				} else if (line.matches(reStart + "BEGIN" + reEnd)) {
					isFunctionBody = true;
				} else {
					listHeader.add(line);
				}

				// Print header
				if (isFunctionBody) {
					this.printHeader(listHeader);
					listHeader.clear();
					isFunctionHeader = false;
					Logger.printInfoEnd("done");
				}

			} else if (isFunctionBody) {
				if (line.matches(reStart + "END" + reStart + functionName + reEnd)) {
					isPrivateSector = false;
					isFunctionBody = false;
					Logger.printInfoEnd("finished");
				}
			} else {
				// Search Function
				if (line.matches(reStart + "FUNCTION" + reEnd) || line.matches(reStart + "PROCEDURE" + reEnd)) {

					// Get function name
					String lineLower = line.toLowerCase();
					if (lineLower.contains("function")) {
						functionName = line.substring(lineLower.indexOf("function") + "function".length());
					} else {
						functionName = line.substring(lineLower.indexOf("procedure") + "procedure".length());
					}
					functionName = functionName.trim();
					int koniec = functionName.indexOf("(");
					if (koniec > 0) functionName = functionName.substring(0, koniec);
					functionName = functionName.trim();
					Logger.printInfoStart("Function: '" + functionName + "'");

					if (!isPrivateSector) {
						Logger.printInfoStart("Header printing ...");
						isFunctionHeader = true;
						listHeader.add(line);
					} else {
						isFunctionBody = true;
					}
				}
			}

			this.writer.flush();
			line = this.reader.readLine();
			Logger.printInfoDetail("line> " + line);
		} // all file lines

		this.writer.write("END " + packageName + ";");
		this.writer.newLine();
		this.writer.newLine();
		this.writer.write("-- Do not edit! Auto generated code " + this.actualTime + " from package body '" + packageName + "'");

		this.finishFiles();
		Logger.printInfoEnd("Processing finished.");
	}

	/**
	 * Prints Header to file.
	 *
	 * @param listPrint what to print
	 * @throws IOException
	 */
	private void printHeader(List<String> listPrint) throws IOException {
		Iterator<String> iterator = listPrint.iterator();
		int counter = 0;
		while (iterator.hasNext()) {
			counter++;
			String codeOnly = iterator.next();
			int position = codeOnly.indexOf("--");
			if (position >= 0) {
				codeOnly = codeOnly.substring(0, position);
			}
			codeOnly = codeOnly.replaceAll("\\s++$", ""); // Remove trailing whitespace.
			this.writer.write(codeOnly);
			if (counter < listPrint.size()) {
				this.writer.newLine();
			}
		}
		this.writer.write(";");
		this.writer.newLine();
		this.writer.newLine();
	}

	/**
	 * Prepare files to read and write.
	 *
	 * @throws IOException
	 */
	private void startFiles() throws IOException {
		// Prepare files to read and write.
		this.reader = new BufferedReader(new FileReader(this.readPath));
		this.writer = new BufferedWriter(new FileWriter(this.writePath));
	}

	/**
	 * Close files.
	 *
	 * @throws IOException
	 */
	private void finishFiles() throws IOException {
		// Close files.
		this.reader.close();
		this.writer.close();
	}

}
