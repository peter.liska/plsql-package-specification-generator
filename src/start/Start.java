package start;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import basic.Logger;
import basic.WorkingData;
import generate.Generator;

public class Start {

	public static void main(String[] args) {

		if (args.length < 1) {
			System.out.println("No input arguments! Please specify input file with *.pkb to generate as first argument.");
			System.out.println("Type 'log' as second argument if you want create debug.log");
			System.exit(-1);
		}
		if (args.length > 1) {
			if (args[1].contains("log")) {
				Logger.info = true;
				Logger.info_detail = false;
			}
		}

		// Basics
		Logger.printInfoStart("Package Specification Generator");
		System.setProperty("line.separator", "\n");

		// Get Changes
		List<String> listPackageBody = null;
		try {
			Logger.printInfoStart("Reading changes from file: '" + args[0] + "'");
			listPackageBody = WorkingData.getChangesFromFile(args[0]);
			Logger.printInfoEnd("Changes ready.");
		} catch (IOException ioEx) {
			Logger.printError(ioEx.toString());
			System.exit(-1);
		}

		// All files one by one
		Iterator<String> iterator = listPackageBody.iterator();
		while (iterator.hasNext()) {
			String fullToRead = iterator.next();
			Logger.printInfoStart("Working on file: '" + fullToRead + "'");

			String pathToRead = fullToRead.substring(0, fullToRead.lastIndexOf("/") + 1);
			String fileToRead = fullToRead.substring(fullToRead.lastIndexOf("/") + 1);
			String packageName = fileToRead.substring(0, fileToRead.indexOf('.'));
			String fullToWrite = pathToRead + packageName + ".pks";

			Logger.printInfo("packageName  : '" + packageName + "'");
			Logger.printInfo("file to write: '" + fullToWrite + "'");

			Generator generator = new Generator(fullToRead, fullToWrite);
			try {
				generator.doWork();
			} catch (IOException ioEx) {
				Logger.printError(ioEx.toString());
				System.exit(-1);
			}
			Logger.printInfoEnd("File done.");
		} // all files
		Logger.printInfoEnd("Generated successufully.");
	}
}